import React from 'react'
import './App.css'
import RenderWithMap from './Components/RenderWithMap/RenderWithMap'
// import ConditinalRendering from './Components/ConditinalRendering/ConditinalRendering'
// import DemoState from './Components/DemoState/DemoState'
// import Ex_Redux_Toolkit from './Components/Ex_Redux_Toolkit/Ex_Redux_Toolkit'
// import EventHandLing from './Components/EventHandLing/EventHandLing'

const App: React.FC = () => {

  return (
    <div>
      {/* <Ex_Redux_Toolkit /> */}
      {/* <EventHandLing/> */}
      {/* <ConditinalRendering /> */}
      {/* <DemoState/> */}
      <RenderWithMap/>
    </div>
  )
}

export default App
