import React, { useState } from 'react'

const ConditinalRendering: React.FC = () => {
  const [isLogin, setIsLogin] = useState(false);  

  const handleLogin = () :void => {
    console.log('before', isLogin);
    setIsLogin(true);
    console.log('after', isLogin);
  }

  const handleLogout = () :void => {
    console.log('before', isLogin);
    setIsLogin(false);
    console.log('after', isLogin);
  }

  const renderContentButton = ()  => {
     if(isLogin){
        return(
            <button onClick={handleLogout} 
            className='bg-violet-500  
            text-white rounded-md px-4 py-2 cursor-pointer 
            hover:bg-violet-600 
            active:bg-violet-700'>Logout</button>
        );
     } else{
        return(
            <button onClick={handleLogin} 
            className='bg-violet-500  
            text-white rounded-md px-4 py-2 cursor-pointer 
            hover:bg-violet-600 
            active:bg-violet-700'>Login</button>
        )
     }
  }

return (
    <div>
        <h2>ConditinalRendering</h2>
          {renderContentButton()}
    </div>
  )
}

export default ConditinalRendering