import React from 'react'
import List from './List'
import Add from './Add'
import Products from './Products'
import Cart from './Cart'

const Ex_Redux_Toolkit: React.FC = () => {
  return (
    <div>
      <Add/>
      <List/>
      <Products/>
      <Cart/>
    </div>
  )
}

export default Ex_Redux_Toolkit