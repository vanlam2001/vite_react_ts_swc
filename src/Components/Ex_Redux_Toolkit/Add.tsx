import React, {useRef} from 'react'
import { useAppDispatch } from '../../store/store';
import { addPerson } from '../../store/features/personSlice';

const Add: React.FC = () => {
  const name = useRef<string>("");
  console.log(name)
  const dispatch = useAppDispatch();
  return (
    <div className='border rounded-md p-2 shadow-md m-2'>
     <label htmlFor="">Person Name:</label>
     <input
       onChange={(e) => (name.current = e.target.value)} 
      className='border rounded-md p-2 mx-2' type="text" />
     <button
     onClick={() => dispatch(addPerson({name: name.current}))}
     className='bg-violet-500  text-white rounded-md px-4 py-2 cursor-pointer hover:bg-violet-600 active:bg-violet-700'>Add</button>
    </div>
  )
}

export default Add