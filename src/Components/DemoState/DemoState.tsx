import React, { useState } from 'react'

const DemoState: React.FC = () => {
    const [userName, setUserName] = useState('alice');
    const [ageUserName, setAgeUserName] = useState(18);
    
    const handleChangeUserName = (): void => {
       let name;
       if(userName === 'bob'){
          name = 'alice';
       } else{
         name = 'bob';
       }
       setUserName(name);
       console.log(name);
    }

    const handleChangeAgeUserName = () :void =>{
        let age;
        if(ageUserName === 20){
             age = 18;
        } else{
            age = 20;
        }
        setAgeUserName(age);
        console.log(age)
    }
    

    return (
    <div>
        <h2>DemoString</h2>
        <h3 className={userName == 'bob' ? 'text-red-600' : 'text-purple-600'}>{userName}</h3>
        <button onClick={handleChangeUserName}
            className='bg-violet-500  
            text-white rounded-md px-4 py-2 cursor-pointer 
            hover:bg-violet-600 
            active:bg-violet-700'>Change UserName</button>

        <h2>DemoNumber</h2>
        <h3 className={ageUserName == 20 ? 'text-amber-600' : 'text-red-600'}>{ageUserName}</h3>
        <button
        onClick={handleChangeAgeUserName}
        className='bg-violet-500  
            text-white rounded-md px-4 py-2 cursor-pointer 
            hover:bg-violet-600 
            active:bg-violet-700'>Change AgeUserName</button>


        
    </div>
  )
}

export default DemoState