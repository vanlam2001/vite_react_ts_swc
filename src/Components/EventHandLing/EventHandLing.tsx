import React from 'react'

const EventHandLing: React.FC = () => {
  const handleLogin = (): void => {
    console.log('yes');
  }

  const handleSayHelloWithName = (userName: string, age: number) :void=> {
       console.log('hello', userName, age);
  }

  const handleMyClass = (classRoom: string, quantity: number, teacher: string) :void =>  {
        console.log('hello',  classRoom, quantity, teacher);
  }

  const testParameters = (number1: number, number2: number, number3: number, number4: number) :void => {
    console.log('hello',  number1, number2, number3, number4);
  }
  
    return (
    <div>
        <h2>EventHandLing</h2>
        <button onClick={handleLogin} 
        className='bg-violet-500  
        text-white rounded-md px-4 py-2 cursor-pointer 
        hover:bg-violet-600 
        active:bg-violet-700'>Login</button>

  <button onClick={() => {handleSayHelloWithName('Alice', 18)}}
        className='bg-violet-500  
        text-white rounded-md px-4 py-2 cursor-pointer 
        hover:bg-violet-600 
        active:bg-violet-700'>Say Hello with Me</button>
  
  <button onClick={() => {handleMyClass('A12', 40, 'Alice')}}
        className='bg-violet-500  
        text-white rounded-md px-4 py-2 cursor-pointer 
        hover:bg-violet-600 
        active:bg-violet-700'>Say Hello ClassRoom Me</button>
    
    <button onClick={() => {testParameters(1, 2, 3, 4)}}
        className='bg-violet-500  
        text-white rounded-md px-4 py-2 cursor-pointer 
        hover:bg-violet-600 
        active:bg-violet-700'>Say Hello ClassRoom Me</button>
   
    </div>

    

    

  )
}

export default EventHandLing